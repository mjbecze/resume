
;; Run 'guix build -f guix.scm' to build the web site.
(use-modules (guix)
	     (gnu)
             (guix modules)
	     (guix build-system guile)
	     (gnu packages guile-xyz)
             ((gnu packages guile) #:select (guile-3.0))
             ((guix licenses) #:prefix license:)
             (guix git-download))

(define emacs
  (specification->package "emacs"))

(define rubber
  (specification->package "rubber"))
(define texlive
  (specification->package "texlive-bin"))

(define this-directory
  (dirname (current-filename)))

(define source
  (local-file this-directory "orgs-resume"
              #:recursive? #t
              #:select? (git-predicate this-directory)))

(define build
  (with-imported-modules (source-module-closure
                          '((guix build utils)
			    ((gnu packages guile) #:select (guile-3.0))))
    #~(begin
        (use-modules (guix build utils))
        (copy-recursively #$source ".")

        (and (zero? (system* #+(file-append emacs "/bin/emacs")
                             "--batch" "--no-init-file" "--visit" "./resume.org" "--funcall" "org-latex-export-to-latex"))
	     (zero? (system* #+(file-append rubber "/bin/rubber")
                             "-d" "./resume.tex"))
             (begin
               (mkdir-p #$output)
               (copy-file "./resume.tex" (string-append #$output "/resume.tex")))))))

(computed-file "emacs-build-resume" build)
