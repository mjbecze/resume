{
  description = "A flake for building an Org file to LaTeX document";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = {
    nixpkgs,
    flake-utils,
    ...
  }:
    flake-utils.lib.eachDefaultSystem (
      system: let
        pkgs = nixpkgs.legacyPackages.${system};
        inherit (pkgs) emacs rubber;
        texlive = pkgs.texlive.combined.scheme-full;
      in {
        packages.default = pkgs.stdenv.mkDerivation {
          name = "emacs-build-resume";
          src = ./.;

          buildInputs = [emacs rubber texlive];

          buildPhase = ''
            cp -r $src/* .
            ${emacs}/bin/emacs --batch --no-init-file --visit ./resume.org --funcall org-latex-export-to-latex
            ${rubber}/bin/rubber -d ./resume.tex
          '';

          installPhase = ''
            mkdir -p $out
            cp ./resume.pdf $out/
          '';
        };
      }
    );
}
